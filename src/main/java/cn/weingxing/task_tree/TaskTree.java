package cn.weingxing.task_tree;

import java.util.List;

public abstract class TaskTree {
    private TaskTree child; // 孩子节点
    private TaskTree sibling; // 兄弟节点

    // 节点处理过程中要用到的数据
    protected Long id;
    protected List ids;
    protected Boolean result; // 处理结果


    public TaskTree() {
        this.id = null;
        this.child = null;
        this.ids = null;
        this.sibling = null;
        this.result = true;
    }

    public TaskTree(Long id, List ids) {
        this.id = id;
        this.ids = ids;
        this.result = true;
        this.sibling = null;
        this.child = null;
    }

    public TaskTree child() {
        return this.child;
    }

    public TaskTree sibling() {
        return this.sibling;
    }

    public TaskTree setChild(TaskTree node) {
        this.child = node;
        return node;
    }

    public TaskTree setSibling(TaskTree node) {
        this.sibling = node;
        return node;
    }

    // 传播函数，用于向孩子节点、兄弟节点广播数据
    protected Boolean propagation(TaskTree sibling, TaskTree child,
                                  Long id, List ids,
                                  List nextIds, Boolean result) {
        if(sibling != null && child != null)
            return child.doDelete(null, nextIds, result) && sibling.doDelete(id, ids, result);
        else if(child != null) return child.doDelete(null, nextIds, result);
        else if(sibling != null) return sibling.doDelete(id, ids, result);
        else return result;
    }

    // 业务函数，具体在Handler中实现
    public abstract Boolean doDelete(Long id, List ids, Boolean result);
}
