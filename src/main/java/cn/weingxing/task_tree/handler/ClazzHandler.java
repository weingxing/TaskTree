package cn.weingxing.task_tree.handler;

import cn.weingxing.task_tree.TaskTree;

import java.util.ArrayList;
import java.util.List;

public class ClazzHandler extends TaskTree {
    public ClazzHandler() {
        super();
    }

    public ClazzHandler(Long id, List ids) {
        super(id, ids);
    }

    @Override
    public Boolean doDelete(Long id, List ids, Boolean result) {
        TaskTree sibling = super.sibling();
        TaskTree child = super.child();
        System.out.println("\n\nClazzHandler：");
        // 如果是根节点，这里是个例子，具体如何判断要根据实际业务来定
        if (ids == null) {
            System.out.println("根据ID查询下级所需id数据：" + id);
            List<Integer> nextIds = new ArrayList<Integer>();
            nextIds.add(2122);
            nextIds.add(3313);
            // 模拟操作成功，记录结果
            result = result && true;
            System.out.println("根据ID查询并删除班级数据：" + id);
            return super.propagation(sibling, child, id, ids, nextIds, result);
        } else {
            // 不是根节点
            System.out.println("根据ids查询下级所需数据");
            List<Integer> nextIds = new ArrayList<Integer>();
            nextIds.add(2612);
            nextIds.add(3351);
            nextIds.add(2253);
            // 模拟操作成功，记录结果
            result = result && true;
            System.out.println("根据上级传来的ids删除多条班级数据：");
            for(Object i : ids) {
                System.out.print(i + "\t");
            }
            return super.propagation(sibling, child, id, ids, nextIds, result);
        }
    }
}
