package cn.weingxing.task_tree.handler;

import cn.weingxing.task_tree.TaskTree;

import java.util.ArrayList;
import java.util.List;

public class CollegeHandler extends TaskTree {
    public CollegeHandler() {
        super();
    }

    public CollegeHandler(Long id, List ids) {
        super(id, ids);
    }

    @Override
    public Boolean doDelete(Long id, List ids, Boolean result) {
        TaskTree sibling = super.sibling();
        TaskTree child = super.child();

        System.out.println("\n\nCollegeHandler：");

        // 顶级节点
        if (ids == null) {
            System.out.println("根据ID查询下级id数据：" + id);
            List<Integer> nextIds = new ArrayList<Integer>();
            nextIds.add(222);
            nextIds.add(333);
            // 模拟操作成功，记录结果
            result = result && true;
            System.out.println("根据ID查询并删除学院数据：" + id);
            return super.propagation(sibling, child, id, ids, nextIds, result);
        } else {
            System.out.println("根据上级传来的ids删除多条学院数据：");
            for(Object i : ids) {
                System.out.print(i + "\t");
            }
            System.out.println("根据ids查询下级所需数据");
            List<Integer> nextIds = new ArrayList<Integer>();
            nextIds.add(212);
            nextIds.add(331);
            nextIds.add(223);
            // 模拟操作成功，记录结果
            // 模拟操作成功，记录结果
            result = result && true;
            System.out.println("根据上级传来的ids删除多条专业数据：");
            for(Object i : ids) {
                System.out.print(i + "\t");
            }
            return super.propagation(sibling, child, id, ids, nextIds, result);
        }
    }
}
