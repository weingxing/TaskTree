package cn.weingxing.task_tree.handler;

import cn.weingxing.task_tree.TaskTree;

import java.util.ArrayList;
import java.util.List;

public class ExamHandler extends TaskTree {
    public ExamHandler() {
        super();
    }

    public ExamHandler(Long id, List ids) {
        super(id, ids);
    }

    @Override
    public Boolean doDelete(Long id, List ids, Boolean result) {
        TaskTree sibling = super.sibling();
        TaskTree child = super.child();
        System.out.println("\n\nExamHandler：");

        // 顶级节点
        if (ids == null) {
            System.out.println("根据ID查询下级id数据：" + id);
            List<Integer> nextIds = new ArrayList<Integer>();
            nextIds.add(224562);
            nextIds.add(456333);
            // 模拟操作成功，记录结果
            result = result && true;
            System.out.println("根据ID查询并删除考试数据：" + id);
            return super.propagation(sibling, child, id, ids, nextIds, result);
        } else {
            System.out.println("根据ids查询下级所需数据");
            List<Integer> nextIds = new ArrayList<Integer>();
            nextIds.add(214562);
            nextIds.add(334561);
            nextIds.add(45623);
            // 模拟操作成功，记录结果
            result = result && true;
            System.out.println("根据上级传来的ids删除多条考试数据：");
            for(Object i : ids) {
                System.out.print(i + "\t");
            }
            return super.propagation(sibling, child, id, ids, nextIds, result);
        }
    }
}
