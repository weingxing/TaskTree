package cn.weingxing.task_tree;

import cn.weingxing.task_tree.handler.*;

public class Main {
    public static void main(String[] args) {
        TaskTree tree = new CollegeHandler();
        tree.setChild(new MajorHandler())
                .setChild(new ClazzHandler())
                .setChild(new ExamHandler())
                .setSibling(new UserHandler());
        System.out.println("\n\n最终结果：" + tree.doDelete(1L, null, true));
    }
}
